## [0.4.1] - 2024-12-09

### Added

- Ajout d'un fichier AUTHORS pour le dépôt de niiif-niiif sur HAL.


## [0.4.0] - 2024-11-04

### Changed

- Suite à la modification du format de la citation des données Nakala, la citation ne peut plus être utilisée comme label du manifeste.
- Les labels du manifeste doivent être précisés avec le paramètre -label.
- Le nom du déposant n'est plus utilisé pour la propriété IIIF requiredStatement. Le label et la valeur de cette propriété peuvent être précisés avec les paramètres -rslabel et -rsvalue.

## [0.3.1] - 2021-12-09

### Fixed

- Correction d'un bug d'affichage des images au format TIFF (reproduit avec UniversalViewer 4.x).

## [0.3.0] - 2021-11-16

### Added

- Affichage d'une barre de progression des traitements.
- Affichage de l'URL du manifeste à transmettre à une visionneuse IIIF.

## [0.2.0] - 2021-11-09

### Fixed

- Mise à jour des manifestes vers IIIF Presentation API 3.0. 

### Added

- Ajout d'un paramètre optionnel -behavior paged permettant l'affichage paginé des documents (voir la propriété [behavior ](https://beta.iiif.io/api/presentation/3.0/#32-technical-properties) de IIIF Presentation API 3.0).

## [0.1.0] - 2021-10-22

### Fixed

- niiif-niiif fonctionne désormais sur les données et les collections non publiées (déposées).

## [0.0.9b0] - 2021-09-28

### Changed

- Adaptation du code pour utiliser niiif-niiif dans un script Python.

## [0.0.8b0] - 2021-07-09

### Fixed

- Prise en compte de la possibilité d'erreur de création du manifeste.

### Changed

- Prise en compte de la mise à jour 1.0.1 de l'API Nakala pour la récupération du propriétaire du fichier.  

## [0.0.7b0] - 2021-07-08

### Changed

- Distribution du module sous forme de package pour PyPI.
- Suppression du fichier settings.py. La clé d'API de l'utilisateur Nakala doit être passé en paramètre.

## [0.0.6b0] - 2021-07-06

### Changed

- Mise à jour de setup.py pour tenter de régler le problème d'affichage des accents de la description du projet sur TestPyPI.

## [0.0.5b0] - 2021-07-06

### Changed

- Mise à jour de setup.py pour test sur TestPyPI.

## [0.0.4b0] - 2021-06-23

### Changed

- Suppression préalable du manifeste s'il existe déjà (un fichier metadata.json dont la description est "IIIF manifest ").

## [0.0.3b0] - 2021-06-23

### Added

- Gestion de la saisie d'un identifiant de donnée Nakala erroné.

## [0.0.2b0] - 2021-06-22

### Added

- L'identifiant de la donnée Nakala peut-être passée avec le paramètre ```-dataid``` ou ```--data_identifier```
- Affichage d'une aide avec le paramètre ```-h```.

## [0.0.1b0] - 2021-06-09

### Added

- Création d'un manifeste IIIF à partir des fichiers TIFF ou JPEG d'une donnée Nakala dont l'identifiant est spécifié dans la variable ``dataIdentifier``` de main.py puis ajout du manifeste à la donnée Nakala. Le dépôt du fichier nécessite d'avoir un compte Nakala avec des droits d'écriture sur la donnée et la saisie de la clé d'API Nakala dans un fichier settings.py à créer au même niveau que main.py) :
  
```Python
dataIdentifier = '10.34847/nkl.66bdx361'
```
